import React, { Component } from 'react'

class Company extends Component {
  render() {
  	let {item} = this.props
    return (
      <div>
    		Name {item.name} with {item.employees} employees {item.revenue} revenue
    		<button value = "select" onClick={this.select}></button>
      </div>
    )
  }
}

export default Company
