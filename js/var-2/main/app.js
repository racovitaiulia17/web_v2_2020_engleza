class Duck{
    constructor(name){
        this.name = name
        if(!name || typeof name != "string"){
            throw new Error("name must be string or String");
        }
    }
    move(){
        return `${this.name} is moving`
    }
    swim(){
        return `${this.name} is swimming`
    }
}


class RubberDuck extends Duck{
    swim(){
        return `${this.name} can't swim, only float`
    }
    
    float(){
        return `${this.name} floats`
    }
    
}



const app = {
    Duck,
    RubberDuck
}

module.exports = app